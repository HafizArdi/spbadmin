@include('admin.components.header')
@include('admin.components.navbar')
@include('admin.components.sidebar')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Add Account</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('Admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('Admin.users.index') }}">User Management</a></li>
                        <li class="breadcrumb-item active">Add Account</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <form action="{{ route('Admin.users.store') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text"
                                        class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}"
                                        name="username" id="username" placeholder="Enter username"
                                        value="{{ old('username') }}">
                                    @if ($errors->has('username'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('username') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <div class="input-group date">
                                        <input type="password"
                                            class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                                            name="password" id="password" placeholder="Password">
                                        <div class="input-group-append" id="togglePassword">
                                            <div class="input-group-text"><i id="eye" style="cursor: pointer"
                                                    class="fas fa-eye-slash"></i></div>
                                        </div>
                                        @if ($errors->has('password'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="jumlah_user">Total Activation Account</label>
                            <input type="number" min=1
                                class="form-control {{ $errors->has('jumlah_user') ? 'is-invalid' : '' }}"
                                name="jumlah_user" id="jumlah_user" placeholder="Enter username" value="1">
                            @if ($errors->has('jumlah_user'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('jumlah_user') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="expiredate">Expired Date</label>
                            <div class="input-group date" id="expiredate" data-target-input="nearest">
                                <input type="text"
                                    class="form-control datetimepicker-input {{ $errors->has('expiredate') ? 'is-invalid' : '' }}"
                                    name="expiredate" data-target="#expiredate" data-toggle="datetimepicker" />
                                <div class="input-group-append" data-target="#expiredate" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                                @if ($errors->has('expiredate'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('expiredate') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="d-block px-4 mb-4">
                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!--/. container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Date -->
@include('admin.components.footer')
