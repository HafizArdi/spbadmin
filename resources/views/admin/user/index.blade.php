@include('admin.components.header')
@include('admin.components.navbar')
@include('admin.components.sidebar')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">User Management</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('Admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">User Management</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    @if (Session::has('success'))
        <div class="mx-4">
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
                @endphp
            </div>
        </div>
    @endif
    <!-- /.content-header -->
    <div class="card mx-3">
        <div class="card-header">
            <a href="{{ route('Admin.users.add') }}" class="btn btn-primary"><i class="fas fa-user-plus mr-1"></i> Add
                Account</a>
        </div>
        <div class="card-body px-2 pb-2">
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover" id="datatable">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>#</th>
                                        <th>USERNAME</th>
                                        <th>Activation Account</th>
                                        <th>CREATED AT</th>
                                        <th>EXPIRED DATE</th>
                                        <th>STATUS</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@include('admin.components.footer')
