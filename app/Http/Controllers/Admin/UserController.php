<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Session;
use App\Models\UserShopee;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use DataTables;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $todays = date("Y-m-d");
        $users = Customer::select();
        if($request->ajax()) {
            return DataTables::eloquent($users)
            ->addIndexColumn()
            ->addColumn('activationAccount', function($user) {
                return $user->jumlah_user;
            })
            ->addColumn('formattedDate', function($user) {
                return Carbon::parse(date('d M Y', strtotime($user->exp_date)))->translatedFormat('d M Y');
            })
            ->addColumn('createdAt', function($user) {
                return Carbon::parse(date('d M Y', strtotime($user->reg_date)))->translatedFormat('d M Y');
            })
            ->addColumn('status', function($user) {
                $expired = $user->exp_date;
                $todays = date("Y-m-d");
                if($todays > $expired) {
                    return '<h6><span class="badge badge-danger">Expired</span></h6>';
                } else {
                    return '<h6><span class="badge badge-success">Active</span></h6>';
                }
            })
            ->addColumn('action', function($user) {
                //  action="{{ route('blog.destroy', $blog->id) }}"
                $html = '<form onsubmit="return confirm("Apakah Anda Yakin ?");" action="'.route('Admin.users.destroy', $user->user_id).'" method="POST">
                            <a href="'.route('Admin.users.detail', $user->user_id).'" class="btn btn-sm btn-info">Detail</a>
                            <a href="'.route('Admin.users.edit', $user->user_id).'" class="btn btn-sm btn-dark">Edit</a>
                            <input type="hidden" name="_token" value="'. csrf_token() .'">
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>';
                return $html;
            })
            ->rawColumns(['status', 'action'])
            ->toJson();
        }
        return view('admin.user.index');
    }

    public function create()
    {
        return view('admin.user.add');
    }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'username' => 'required|min:3|unique:user,username',
            'password' => 'required|min:5',
        ]);

        $formattedDate = date("Y-m-d", strtotime($request->expiredate));
        $hashed = md5($request->password);

        $account = Customer::create([
            'username' => $request->username,
            'password' => $hashed,
            'exp_date' => $formattedDate,
            'reg_date' => Carbon::parse(date("Y-m-d")),
            'jumlah_user' => $request->jumlah_user
        ]);

        $session = new Session();
        $session->user_id = $account->user_id;
        $session->save();

        return redirect()->route('Admin.users.index')->with('success', 'Account added');
    }

    public function show(Request $request, $id)
    {
        $user_shopee = UserShopee::where('user_id', $id)->get();
        $customer = Customer::find($id);

        if($request->ajax()) {
            $user_shopee = UserShopee::where('user_id', $id);

            return DataTables::eloquent($user_shopee)
            ->addIndexColumn()
            ->addColumn('shopee_account', function($user_shopee) {
                return $user_shopee->shopee_account;
            })
            ->addColumn('action', function($user_shopee) {
                //  action="{{ route('blog.destroy', $blog->id) }}"
                $html = '<form onsubmit="return confirm("Apakah Anda Yakin ?");" action="'.route('Admin.users_shopee.destroy_account', $user_shopee->id).'" method="POST">
                            <a href="'.route('Admin.users_shopee.edit_account', $user_shopee->id).'" class="btn btn-sm btn-dark">Edit</a>
                            <input type="hidden" name="_token" value="'. csrf_token() .'">
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>';
                return $html;
            })
            ->rawColumns(['status', 'action'])
            ->toJson();
        }
        return view('admin.user.detail', compact('user_shopee', 'customer'));
    }

    public function edit($id)
    {
        $user_data = Customer::findOrFail($id);
        return view('admin.user.edit', compact('user_data'));
    }

    public function update(Request $request, $id)
    {
        $formattedDate = date("Y-m-d", strtotime($request->editdate));
        Customer::find($id)->update([
            'username' => $request->username,
            'exp_date' => $formattedDate,
            'jumlah_user' => $request->jumlah_user
        ]);
        return redirect()->route('Admin.users.index')->with('success', 'Customer update');
    }

    public function destroy($id)
    {
        Customer::find($id)->delete();
        return redirect()->route('Admin.users.index')->with('success', 'Customer deleted');
    }

    public function create_account($id)
    {
        $customer = Customer::find($id);
        return view('admin.account_shopee.add', compact('customer'));
    }

    public function store_account(Request $request)
    {
        $user = Customer::where('username', $request->username)->first();
        $get_shopee_account = UserShopee::where('user_id', $user->user_id)->get();

        if (count($get_shopee_account) >= $user->jumlah_user) {
            return redirect()->route('Admin.users.detail', $user->user_id)->with('error', 'Account Shopee Telah Mencapai Batas Maksimum');
        }else{
            $shopee_account = UserShopee::create([
                'user_id' => $user->user_id,
                'shopee_account' => $request->shopee_account,
            ]);
        }

        return redirect()->route('Admin.users.detail', $user->user_id)->with('success', 'Account Shopee added');
    }

    public function destroy_account($id)
    {
        $user_shopee = UserShopee::find($id);
        $user = Customer::find($user_shopee->user_id);

        $user_shopee->delete();
        return redirect()->route('Admin.users.detail', $user->user_id)->with('success', 'Customer deleted');
    }

    public function edit_account($id)
    {
        $user_shopee = UserShopee::where('id', $id)->first();
        $customer = Customer::find($user_shopee->user_id);

        return view('admin.account_shopee.edit', compact('user_shopee', 'customer'));
    }

    public function update_account(Request $request, $id)
    {
        $user_shopee = UserShopee::find($id);
        $customer = Customer::find($user_shopee->user_id);

        $user_shopee->shopee_account = $request->shopee_account;
        $user_shopee->save();

        return redirect()->route('Admin.users.detail', $customer->user_id)->with('success', 'Customer Updated');
    }
}
